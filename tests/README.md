## Testing

Once the *SSA-FBA* extension module has been built, test simulations can be performed on a generic [cobra](https://opencobra.github.io/cobrapy/) object representing the FBA sub-model. The tests are illustrated using the method *test()* in the [module](./test_module.py), which loads the supplied SBML model [Metabolism](./Metabolism.xml) and performs two tests:

* Test 1 (called using method *test_speed()*) creates a random sequence of 10000 events by randomly selecting (for each event) 1-5 reactions whose upper bounds are known (i.e., not zero or infinity in the FBA sub-model). It then executes two simulations in succession, looping over the event sequence and reducing the upper bound value of each specified reaction in that event by multiplying its current upper bound value by 0.9. The new upper bound values are used to construct an optimal flux distribution and the corresponding forward growth rate is printed to a results file. Simulation 1 performs this procedure using the naive approach of re-solving the linear programming (LP) problem after each upper bound is updated. Simulation 2 implements the fast algorithm described in the documentation. Arguments for *module.test()* can be provided to specify sequences of different lengths (*num events*), different maximum number of reactions per event (*max num per event*), and multiplicative factors (*omega*) in place of the default values 10000, 5, and 0.9, respectively

* Test 2 (called using method *toy_simulation()*) runs Algorithm 1 on an extremely simple SSA-FBA model, using the supplied GLPK column indices to specify which SSA-FBA and FBA-only reactions should be used to calculate and bound the lumped reaction v_2

The method *simple_model()* in the [module](./test_module.py) demonstrates how to build the toy model and run the simulation, using *Python*

