FROM ubuntu:19.10

# BioContainers-style metadata
LABEL base_image="ubuntu:19.10"
LABEL version="3.7"
LABEL software="ssafba"
LABEL software.version="3.7"
LABEL about.summary="Stochastic Simulation Algorithm with Flux-Balance Analysis embedded"
LABEL about.home="https://gitlab.com/davidtourigny/single-cell-fba"
LABEL about.documentation="https://gitlab.com/davidtourigny/single-cell-fba"
LABEL about.license_file="https://gitlab.com/davidtourigny/single-cell-fba/-/blob/main/LICENSE.md"
LABEL about.license="SPDX:GPL-3.0-only"
LABEL about.tags="single-cell,metabolism,gene expression,systems biology,computational biology,discrete kinetic modeling,constraint-based modeling,stochastic simulation algorithm,flux-balance analysis,SSA,FBA"
# LABEL extra.identifiers.biotools="ssafba"
LABEL maintainer="David Tourigny <dst2156@cumc.columbia.edu>"

# arguments and environment variables
ARG NPROC=1
ARG GLPK_VERSION="4.65"

ENV PYTHONUNBUFFERED=1

# working directory
WORKDIR /opt

# install dependencies
RUN set -eux \
    && apt-get update \
    && apt-get install --yes \
        build-essential \
        cmake \
        python3-dev \
        python3-pip \
        libgmp-dev \
        curl \
    && python3 -m pip install --upgrade \
        pip \
        setuptools \
        wheel \
        pytest \
        cobra \
        matplotlib \
        numpy \
        openpyxl \
        scipy \
    && rm -rf /.cache/pip /tmp/pip* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY scripts/ ./

RUN set -eux \
    && bash build_pybind11.sh \
    && bash build_glpk.sh

# install SSA-FBA
COPY . ./

RUN set -eux \
    && python3 -m pip install . \
    && rm -rf /.cache/pip /tmp/pip*
