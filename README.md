# SSA-FBA: stochastic simulation algorithm with flux-balance analysis embedded

This project introduces *SSA-FBA* as described in the paper [Simulating single-cell metabolism using a stochastic flux-balance analysis algorithm](https://www.biorxiv.org/content/10.1101/2020.05.22.110577v1) by David S. Tourigny, Arthur P. Goldberg, and Jonathan R. Karr.

## Building and installation

*SSA-FBA* runs on *Linux* and *OSX*. A [Dockerfile](./Dockerfile) is provided for building a [Docker](https://docs.docker.com/) image to run the software from an interactive container. The image can be built in one step by issuing the command:
```
make build
```
from the root of this repository. It can then be started using
```
make run
```

Alternatively, *SSA-FBA* can be built and installed directly from source once the dependencies (below) have been installed correctly. The package can be installed directly from source from the root of this repository by running the command:
```
pip install .
```

## Dependencies

Building the executable directly from source depends on having the following packages pre-installed on the user's platform:
* [CMake](https://cmake.org/) version 2.8.3 or higher
* [Python](https://www.python.org/) version 3.6 or higher
* [GLPK](https://www.gnu.org/software/glpk/) version 4.65 (depends on [GMP](https://gmplib.org/))
* [pybind11](https://github.com/pybind/pybind11) (depends on the Python module [pytest](https://pypi.org/project/pytest/))
* a suitable *C++* compiler, e.g. [GCC](http://gcc.gnu.org/)

*GLPK* and *pybind11* are most conveniently obtained, built and installed running the scripts [build_glpk](./scripts/build_glpk.sh) and [build_pybind11](./scripts/build_pybind11.sh), respectively, from the root of this repository. The *GLPK* version number variable should be exported prior to running the first script.

