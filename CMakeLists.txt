cmake_minimum_required(VERSION 2.8.3)
cmake_policy(SET CMP0069 NEW)

project(ssa-fba)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "-Wno-deprecated-register" )

################### Find pybind11

set(PYBIND11_PYTHON_VERSION 3)
find_package(pybind11 REQUIRED)

################### Set path for custom FindModules

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

################### Find glpk packages

FIND_PACKAGE(GLPK REQUIRED)
message(STATUS "GLPK_LIBRARIES = ${GLPK_LIBRARIES}")
message(STATUS "GLPK_INCLUDE_DIRS = ${GLPK_INCLUDE_DIRS}")

################### Include directories and libraries

INCLUDE_DIRECTORIES(${GLPK_INCLUDE_DIRS})
LINK_LIBRARIES(${GLPK_LIBRARIES})

################### Source files

set(SRC ./ssafba/core/)
AUX_SOURCE_DIRECTORY(${SRC}/emblp/ EMBLP_FILES)
AUX_SOURCE_DIRECTORY(${SRC}/mersenne/ MERSENNE_FILES)
set(MODULE_FILES ${SRC}core_module.cpp ${SRC}core.cpp ${MERSENNE_FILES} ${EMBLP_FILES})

################### Build module

pybind11_add_module(core ${MODULE_FILES})
