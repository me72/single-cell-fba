#!/bin/bash
for i in $(seq 1000)
do
    echo $i
    /usr/bin/python3 simulate.py --duration 3600 --adk-vmax 0.053
    mv events.txt results/events.txt
    mv results results_1_$i
done

for i in $(seq 1000)
do
    echo $i
    /usr/bin/python3 simulate.py --duration 3600 --adk-vmax 0.53
    mv events.txt results/events.txt
    mv results results_2_$i
done

for i in $(seq 1000)
do
    echo $i
    /usr/bin/python3 simulate.py --duration 3600 --adk-vmax 5.3
    mv events.txt results/events.txt
    mv results results_3_$i
done

for i in $(seq 1000)
do
    echo $i
    /usr/bin/python3 simulate.py --duration 3600 --adk-vmax 53.0
    mv events.txt results/events.txt
    mv results results_4_$i
done
