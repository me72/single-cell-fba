""" Utility functions

:Author: Jonathan Karr <karr@mssm.edu>
:Date: 2020-06-11
:Copyright: 2016-2020, Karr Lab
:License: MIT
"""

import numpy


def nan_minimum(x, y):
    """ Get the element-wise minimum of two arrays, ignoring NaN.

    Args:
        x (:obj:`numpy.ndarray`): first array
        y (:obj:`numpy.ndarray`): second array

    Returns:
        :obj:`numpy.ndarray`: element-wise minimum of two arrays
    """
    return numpy.where(numpy.logical_or(numpy.isnan(y), numpy.logical_and(x <= y, numpy.logical_not(numpy.isnan(x)))), x, y)


def nan_maximum(x, y):
    """ Get the element-wise maximum of two arrays, ignoring NaN.

    Args:
        x (:obj:`numpy.ndarray`): first array
        y (:obj:`numpy.ndarray`): second array

    Returns:
        :obj:`numpy.ndarray`: element-wise maximum of two arrays
    """
    return numpy.where(numpy.logical_or(numpy.isnan(y), numpy.logical_and(x >= y, numpy.logical_not(numpy.isnan(x)))), x, y)
