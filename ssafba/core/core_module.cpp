// Copyright (C) 2019, 2020 Columbia University Irving Medical Center,
//     New York, USA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <fstream>
#include <time.h>
#include "core.h"
#include <dlfcn.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

/*
 *------------------------------------
 * For processing objects from Python.
 *------------------------------------
 */

/*
 * SWIGwrapper for glp_prob * (see https://wiki.python.org/moin/boost.python/HowTo#SWIG_exposed_C.2B-.2B-_object_from_Python ).
 */

struct PySwigprobObject{
    PyObject_HEAD
    glp_prob *ptr;
    const char *desc;
};

/*
 *-----------------------------------
 * Functions to be exposed to Python.
 *-----------------------------------
 */

void test_speed(py::object cobra_model, double omega, unsigned int nevnts, unsigned int max_npevnt)
{
    //Here we get SWIGLPK wrapped glp_prob * attribute of Python cobra model object, rationale: ((PySwigprobObject*) PyObject* )->ptr
    glp_prob *lp = ((PySwigprobObject*) cobra_model.attr("solver").attr("problem").ptr() )->ptr;
    glp_smcp parm; //TODO: obtain copy of LP configuration from Python
    glp_init_smcp(&parm); //init smcp struct
    parm.msg_lev = GLP_MSG_OFF; //turn glpk messaging off
    std::cout << std::endl;
    std::cout << "Performing test of speed" << std::endl;
    test_emblp(lp,parm,omega,nevnts,max_npevnt); //test speed of embedded LP classes
    std::cout << std::endl;
}

void toy_simulation(py::object cobra_model, unsigned int growth_rate_idx, unsigned int uptake_rate_idx)
{
    //Here we get SWIGLPK wrapped glp_prob * attribute of Python cobra model object, rationale: ((PySwigprobObject*) PyObject* )->ptr
    glp_prob *lp = ((PySwigprobObject*) cobra_model.attr("solver").attr("problem").ptr() )->ptr;
    glp_smcp parm; //TODO: obtain copy of LP configuration from Python
    glp_init_smcp(&parm); //init smcp struct
    parm.msg_lev = GLP_MSG_OFF; //turn glpk messaging off
    std::cout << std::endl;
    std::cout << "Performing simulation of toy model" << std::endl;
    test_simulation(lp,parm,growth_rate_idx,uptake_rate_idx); //simulate toy model
    std::cout << std::endl;
}

std::vector< std::vector<double> > simulate_model(
                    py::object cobra_model,             //cobra object for FBA model
                    py::object &ssafba_object,          //reference to SsaFba object from Python
                    std::vector<int64_t> substrates,    //initial substrate counts
                    unsigned int max_events,            //max events
                    double duration,                    //duration
                    unsigned int delta,                 //step size for approximate method (exact: delta = 0)
                    double mu,                          //relative scaling factor for SSA-FBA propensities
                    bool fast,                          //boolean for fast method (fast: fast = 1)
                    unsigned int random_seed,           //random seed
                    py::object user_data                //user data
                    )
{
    //Create results array
    std::vector< std::vector<double> > results;
    std::vector<double> init_substrates(substrates.begin(), substrates.end());
    init_substrates.push_back(0.0);
    init_substrates.push_back(0.0);
    results.push_back(init_substrates);
    
    //Here check model compatible (e.g., check substrates consistent with SsaFba object...)
    SsaFba &model = ssafba_object.cast<SsaFba &>(); //cast reference to SsaFba object
    
    //open shared library and assign function pointers to SsaFba object
    
    void *handle = dlopen("./functionlib.so", RTLD_LAZY); //open the library
    if(!handle){ //if library not found
        std::cerr << "Cannot open dynamic library: " << dlerror() << std::endl;
        dlclose(handle);
    }
    dlerror(); //init error
    std::string ssa_prop_symbl = "ssa_propensities_" + model.name; //create symbol tags
    std::string fba_bnds_symbl = "fba_bounds_" + model.name; //create symbol tags
    model.ssa_propensities = (SsaPropensity) dlsym(handle, ssa_prop_symbl.c_str());  //get symbol
    model.fba_bounds = (FbaBound) dlsym(handle, fba_bnds_symbl.c_str());  //get symbol
    const char *dlsym_error = dlerror(); //set error
    if(dlsym_error){ //if error loading symbols
        std::cerr << "Cannot load symbols: " << dlsym_error << std::endl;
        dlclose(handle);
    }
    
    //Here we get SWIGLPK wrapped glp_prob * attribute of Python cobra model object, rationale: ((PySwigprobObject*) PyObject* )->ptr
    glp_prob *lp = ((PySwigprobObject*) cobra_model.attr("solver").attr("problem").ptr() )->ptr;
    glp_smcp parm; //TODO: obtain copy of LP configuration from Python
    glp_init_smcp(&parm); //init smcp struct
    parm.msg_lev = GLP_MSG_OFF; //turn glpk messaging off
    
    //Here we will simulate
    int solver = simulate(results,lp,parm,model,substrates,mu,max_events,duration,delta,fast,random_seed,&user_data); //simulate
    if(solver == 0){
        std::cout << "Simulation successful" << std::endl;
        dlclose(handle);
    }
    else{
        std::cout << "Simulation unsuccessful" << std::endl;
        dlclose(handle);
    }
    return results;
}

/*
 *----------------
 * Python wrapper.
 *----------------
 */

PYBIND11_MODULE(core, m)
{
    m.doc() = "SSA-FBA Python extension module built using pybind11";
    
    m.def("test_speed", test_speed, "Function that tests speed of EMBLP classes for generic cobra.Model() object.", py::arg("cobra_model"), py::arg("omega") = 0.9, py::arg("nevnts") = 10000, py::arg("max_npevnt") = 5);
    
    m.def("toy_simulation", toy_simulation, "Function that generates and simulates toy model for generic cobra.Model() object.", py::arg("cobra_model"), py::arg("growth_rate_idx"), py::arg("uptake_rate_idx"));
    
    m.def("simulate_model", simulate_model, "Function that simulates SSA-FBA model.", py::arg("cobra_model"), py::arg("ssafba_object"), py::arg("substrates"), py::arg("max_events"), py::arg("duration"), py::arg("delta") = 0, py::arg("mu") = 1.0, py::arg("fast") = 1, py::arg("random_seed") = NULL, py::arg("user_data") = py::none());
    
    py::class_<SsaFba>(m, "SsaFba")
        .def(py::init<std::string, unsigned int , unsigned int , unsigned int>())
        .def("set_stoichiometry", &SsaFba::set_stoichiometry, "Function that assigns stochiometry for each SSA-only or SSA-FBA reaction in SsaFba object", py::arg("rxn_index"), py::arg("metabolites"), py::arg("stoichiometries"))
        .def("set_ssafba_indices", &SsaFba::set_ssafba_indices, "Function that assigns each SSA-FBA reaction in SsaFba object a GLPK column index", py::arg("col_index_list"))
        .def("set_dependencies", &SsaFba::set_dependencies, "Function that assigns indices of dependent SSA-only reactions or FBA bounds to each SSA-only or SSA-FBA reaction in SsaFba object", py::arg("rxn_index"), py::arg("dependents"))
        .def_readonly("name", &SsaFba::name)
        .def("set_reactants", &SsaFba::set_reactants, "Function that assigns indices of reactants to SSA-only reactions or FBA bounds in SsaFba object", py::arg("rxn_or_bnd_index"), py::arg("reactants"))
        .def_readonly("name", &SsaFba::name)
        .def_readonly("stoichiometry_inds", &SsaFba::stoichiometry_inds)
        .def_readonly("stoichiometry_vals", &SsaFba::stoichiometry_vals)
        .def_readonly("dependencies", &SsaFba::dependencies)
        .def_readonly("reactants_inds", &SsaFba::reactants_inds)
        .def_readonly("fba_propensities", &SsaFba::fba_propensities)
        ;
}

