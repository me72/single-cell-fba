// Copyright (C) 2019, 2020 Columbia University Irving Medical Center,
//     New York, USA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <vector>
#include <glpk.h>

class EMBLP
{
protected:
    ///Embedded LP problem data
    ///------------------------
    int nrow, ncol, ntot; //int data
    glp_prob *lp; //pointer to current formulation of LP problem
    glp_smcp parm; //parameters of LP problem

public:
    ///Member functions for embedded LP problem
    ///----------------------------------------
    ///Constructor
    EMBLP(glp_prob *, glp_smcp);
    ///Operations
    virtual int optimize(), set_bounds(std::vector<int>, std::vector<double>);
    ///Return data
    int get_nrow(), get_ncol(), get_ntot();
    ///Return values
    virtual double get_val(int);
    virtual double get_obj_val();
    ///Clear memory
    void clear();
};

class EMBLP_FAST : public EMBLP
{
private:
    ///Fast embedded LP problem data
    ///-----------------------------
    std::vector<double> bounds; //current upper bounds of lpvariables
    std::vector<double> basic_vals; //current values of basic lpvariables
public:
    ///Member functions for fast embedded LP problem
    ///---------------------------------------------
    ///Constructor
    EMBLP_FAST(glp_prob *, glp_smcp);
    ///Operations
    int optimize(), set_bounds(std::vector<int>, std::vector<double>);
    void update_bounds(), store_basic_vals(), update_basic_vals(int, double, std::vector<int>&);
    bool check_basic_bounds(std::vector<int>);
    ///Return values
    double get_val(int);
    double get_obj_val();
};
