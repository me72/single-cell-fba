#!/usr/bin/env bash

# Copyright (C) 2018, 2019 Columbia University Irving Medical Center,
#     New York, USA

set -eux

# Use a default of one core.
: "${NPROC:=1}"

# Download the specified version.
curl -L -O "ftp://ftp.gnu.org/gnu/glpk/glpk-${GLPK_VERSION}.tar.gz"
curl -L -O "ftp://ftp.gnu.org/gnu/glpk/glpk-${GLPK_VERSION}.tar.gz.sig"

# Verify the downloaded archive.
gpg --keyserver keys.gnupg.net --recv-keys 5981E818
gpg --verify "glpk-${GLPK_VERSION}.tar.gz.sig"

# Unpack and install GLPK.
tar -xzf "glpk-${GLPK_VERSION}.tar.gz"
cd "glpk-${GLPK_VERSION}"
./configure --disable-reentrant --with-gmp
make -j "${NPROC}" install
cd ..
rm -rf "glpk-${GLPK_VERSION}"*
