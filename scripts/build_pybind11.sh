#!/usr/bin/env bash

# Copyright (C) 2019 Columbia University Irving Medical Center,
#     New York, USA

set -eux

# Use a default of one core.
: "${NPROC:=1}"

# Download pybind11 master branch.
curl -L -O "https://github.com/pybind/pybind11/archive/master.tar.gz"

# Unpack and install pybind11.
tar -xzf "master.tar.gz"
cd "pybind11-master"
mkdir build
cd build
cmake ..
make install -j "${NPROC}"
cd ../..
rm -rf "pybind11-master" "master.tar.gz"
